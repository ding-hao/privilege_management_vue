import request from '../libs/request'

export function loginByUsername(username, password) {
    const data = {
        userName: username,
        password: password
    };
    return request({
        url: '/auth',
        method: 'post',
        params:data
    })
}

export function logout(username) {
    const data = {
        username:username
    };
    return request({
        url: '/logout',
        method: 'post',
        params: data
    })
}

export function getUserInfo(token) {
    return request({
        url: '/admin/info',
        method: 'get',
        params: { token }
    })
}
