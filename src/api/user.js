import request from '../libs/request'

export function getUserList(page,pageSize) {
    const data = {
        page,
        pagesize:pageSize,
    };
    return request({
        url: '/user/list',
        method: 'post',
        data
    })
}
