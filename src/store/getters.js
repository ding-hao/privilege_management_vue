const getters = {
    token: state => state.user.token,
    access: state => state.user.access,
    lockScreenPassword: state => state.user.lockScreenPassword,
    username: state => state.user.username,
}
export default getters
