import {getToken, removeToken, setToken} from '../../libs/auth'
import {loginByUsername, logout, getUserInfo} from '../../api/login'

const user = {


    state: {
        token: getToken(),
        access: '',
        lockScreenPassword: '',
        username: '',
        randomKey: ''
    },


    mutations: {

        setRandomKey: (state, randomKey) => {
            state.randomKey = randomKey
        },
        setToken: (state, token) => {
            state.token = token
        },

        setAccess: (state, access) => {
            state.access = access
        },

        setLockScreenPassword: (state, lockScreenPassword) => {
            state.lockScreenPassword = lockScreenPassword
        },

        setUsername: (state, username) => {
            state.username = username
        },

    },


    actions: {
        // 用户名登录
        LoginByUsername: ({commit}, userInfo) => {
            const username = userInfo.username.trim();
            return new Promise((resolve, reject) => {
                loginByUsername(username, userInfo.password).then(response => {
                    if (response.data.code != 400) {
                        const token = response.data.token;
                        const randomKey = response.data.randomKey;
                        commit('setToken', token);
                        commit('setUsername', username);
                        commit('setRandomKey', randomKey);
                        setToken(token);
                        console.log(token);
                        // resolve(getUserInfo(token)
                        //     .then(res => {
                        //         commit('setAvator', res.data.data.avatar);
                        //         console.log(res.data.data)
                        //     }))
                        resolve();
                    }
                    // else{
                    //     this.$Message.warning(response.data.message);
                    // }
                }).catch(error => {
                    reject(error)
                })
            })
        },

        // 用户登出
        LogOut({commit, state}) {
            return new Promise((resolve, reject) => {
                logout(state.username).then(() => {
                    commit('setToken', '');
                    removeToken();
                    commit('setAccess', '');
                    // 恢复默认样式
                    let themeLink = document.querySelector('link[name="theme"]');
                    themeLink.setAttribute('href', '');
                    // 清空打开的页面等数据，但是保存主题数据
                    let theme = '';
                    if (localStorage.theme) {
                        theme = localStorage.theme;
                    }
                    localStorage.clear();
                    if (theme) {
                        localStorage.theme = theme;
                    }
                    resolve()
                }).catch(error => {
                    reject(error)
                })
            })
        },

        //根据token获取用户信息
        GetUserInfo({commit, state}, token) {
            token = token.trim();
            console.log('进入' + token)
            return new Promise((resolve, reject) => {
                getUserInfo(token).then(res => {
                    commit('setUsername', res.data.data.name);
                    resolve();
                }).catch(err => {
                    reject(err)
                })

            })
        }
    }
};

export default user
