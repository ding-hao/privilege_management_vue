import Vue from 'vue';
import iView from 'iview';
import Util from '../libs/util';
import VueRouter from 'vue-router';
import Cookies from 'js-cookie';
import store from '@/store'
import { getToken } from '@/libs/auth'
import {routers, otherRouter, appRouter} from './router';

Vue.use(VueRouter);

// 路由配置
const RouterConfig = {
    // mode: 'history',
    routes: routers
};

export const router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    //浏览器上访问显示
    Util.title(to.meta.title);
    //判断屏幕是否已被锁上
    if (Cookies.get('locking') === '1' && to.name !== 'locking') { // 判断当前是否是锁定状态
        next({
            replace: true,
            name: 'locking'
        });//去上锁屏幕
    } else if (Cookies.get('locking') === '0' && to.name === 'locking') {
        next(false);
    } else {
        // if (!Cookies.get('user') && to.name !== 'login') { // 判断是否已经登录且前往的页面不是登录页
        //通过是否存在token来校验不严谨,可以发ajax判断token是否有效,后台会提供判断token是否有效的接口
        console.log('判断是否登录'+getToken())
        if (!getToken() && to.name !== 'login') { // 判断未登录且前往的页面不是登录页
            next({
                name: 'login'
            });
        // } else if (Cookies.get('user') && to.name === 'login') { // 判断是否已经登录且前往的是登录页
        } else if (getToken() && to.name === 'login') { // 判断已经登录且前往的是登录页
            Util.title();
            next({
                name: 'home_index'
            });
        } else {
            // 1.没有登录,去登陆
            // 2.登录,其他页面

            const curRouterObj = Util.getRouterObjByName([otherRouter, ...appRouter], to.name);
            //存在访问页面,而且有权限设置
            if (curRouterObj && curRouterObj.access !== undefined) { // 需要判断权限的路由
                if (curRouterObj.access === parseInt(store.getters.access)) {
                    Util.toDefaultPage([otherRouter, ...appRouter], to.name, router, next); // 如果在地址栏输入的是一级菜单则默认打开其第一个二级菜单的页面
                } else {
                    next({
                        replace: true,
                        name: 'error-403'
                    });
                }
            } else { // 没有配置权限的路由, 直接通过
                Util.toDefaultPage([...routers], to.name, router, next);
            }
        }
    }
});

router.afterEach((to) => {
    Util.openNewPage(router.app, to.name, to.params, to.query);
    iView.LoadingBar.finish();
    window.scrollTo(0, 0);
});
