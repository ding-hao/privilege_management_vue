import Main from '@/views/Main.vue';

// 不作为Main组件的子页面展示的页面单独写，如下
export const loginRouter = {
    path: '/login',
    name: 'login',
    meta: {
        title: 'Login - 登录'
    },
    component: resolve => { require(['@/views/login.vue'], resolve); }
};

export const page404 = {
    path: '/*',
    name: 'error-404',
    meta: {
        title: '404-页面不存在'
    },
    component: resolve => { require(['@/views/error-page/404.vue'], resolve); }
};

export const page403 = {
    path: '/403',
    meta: {
        title: '403-权限不足'
    },
    name: 'error-403',
    component: resolve => { require(['@//views/error-page/403.vue'], resolve); }
};

export const page500 = {
    path: '/500',
    meta: {
        title: '500-服务端错误'
    },
    name: 'error-500',
    component: resolve => { require(['@/views/error-page/500.vue'], resolve); }
};

export const locking = {
    path: '/locking',
    name: 'locking',
    component: resolve => { require(['@/views/main-components/lockscreen/components/locking-page.vue'], resolve); }
};

// 作为Main组件的子页面展示但是不在左侧菜单显示的路由写在otherRouter里
export const otherRouter = {
    path: '/',
    name: 'otherRouter',
    redirect: '/home',
    component: Main,
    children: [
        { path: 'home', title: {i18n: 'home'}, name: 'home_index', component: resolve => { require(['@/views/home/home.vue'], resolve); } },
        { path: 'ownspace', title: '个人中心', name: 'ownspace_index', component: resolve => { require(['@/views/own-space/own-space.vue'], resolve); } },
        { path: 'message', title: '消息中心', name: 'message_index', component: resolve => { require(['@/views/message/message.vue'], resolve); } }
    ]
};

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
    {
        path: '/system',
        icon: 'ios-keypad-outline',
        name: 'system',
        title: '系统管理',
        component: Main,
        children: [
            { path: 'user', icon:'ios-body', title: '用户管理', name: 'user_index', component: resolve => { require(['@/views/system/user/index.vue'], resolve); } },
            // { path: 'user1', icon:'ios-body', title: '用户管理1', name: 'user_index1', component: resolve => { require(['@/views/system/user/user.vue'], resolve); } },
            { path: 'role', icon:'beaker', title: '角色管理', name: 'role_index', component: resolve => { require(['@/views/system/role/index.vue'], resolve); } },
            { path: 'permission', icon:'ios-cloudy', title: '资源管理', name: 'permission_index', component: resolve => { require(['@/views/system/permission/index.vue'], resolve); } },
            { path: 'syslog', icon:'ios-bookmarks-outline', title: '日志管理', name: 'syslog_index', component: resolve => { require(['@/views/system/syslog/index.vue'], resolve); } },
        ]
    },
    {
        path: '/swagger',
        icon: 'ios-bookmarks-outline',
        name: 'swagger',
        title: '接口文档',
        component: Main,
        children: [
            { path: 'index', title: '接口文档', name: 'swagger_index', component: resolve => { require(['@/views/system/swagger/swagger2.vue'], resolve); } },
        ]
    },
];

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
    loginRouter,
    otherRouter,
    locking,
    ...appRouter,
    page500,
    page403,
    page404
];
