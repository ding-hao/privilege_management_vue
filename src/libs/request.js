import axios from 'axios'
import sf from 'string-format'
import Vue from 'vue'
import store from '../store'
import {getToken} from '../libs/auth'


const ajaxUrl = 'http://127.0.0.1:1003';


// 创建一个 axios 实例
const service = axios.create({
    baseURL: ajaxUrl, // api的base_url
    timeout: 30000 // request timeout
});

export const get = (url, params, pathVariable = null) => {
    if (params == null) {
        params = {axios_timestamp_current: new Date().getTime()}
    } else {
        params.axios_timestamp_current = new Date().getTime()
    }
    return service.get(sf(url, pathVariable), {params: params})
};

export const post = (url, params, pathVariable = null) => service.post(sf(url, pathVariable), params);

export const put = (url, params, pathVariable = null) => service.put(sf(url, pathVariable), params);

export const patch = (url, params, pathVariable = null) => service.patch(sf(url, pathVariable), params);

export const del = (url, params, pathVariable = null) => service.delete(sf(url, pathVariable), {params: params});


// 请求拦截
service.interceptors.request.use(config => {
    if (store.getters.token) {
        // 让每个请求携带token-- ['Authorization']为自定义key 请根据实际情况自行修改
        config.headers['Authorization'] = getToken();
    }
    return config
}, error => {
    console.log(error);
    Promise.reject(error);
});

// 响应拦截
service.interceptors.response.use(
    response => {
        const res = response.data;
        if (res.code === 400) {
            /**
             * 当在代码中使用 Vue.use(iView) 的时候，iView 会在 Vue 构造函数的原型上添加一个 $Message 属性，
             * 所以我们才可以在每个组件中使用 this.$Message.error() 方法（ this 指向 Vue 组件实例），
             * 想要在一个单独的文件（非组件）中使用提示，
             * 只需直接从原型上拿到方法即可：
             * import Vue from 'vue'
             * Vue.prototype.$Message.error('请先登录')
             */
            Vue.prototype.$Message.error('账号密码错误');
        }else if(res.code === 402){
            Vue.prototype.$Message.error('用户已退出登录,或者token已失效');
        }else if(res.code === 403){
            Vue.prototype.$Message.error('该用户不存在,修改用户或修改用户角色失败');
        }else if(res.code === 700){
            Vue.prototype.$Message.error('token过期，请重新登录');
        }else if(res.code === 701){
            Vue.prototype.$Message.error('token验证失败');
        }else if(res.code === 702){
            Vue.prototype.$Message.error('签名验证失败');
        }else if(res.code === 703){
            Vue.prototype.$Message.error('参数格式异常');
        }else if(res.code === 704){
            Vue.prototype.$Message.error('参数Json格式异常');
        }else if(res.code === 705){
            Vue.prototype.$Message.error('不能删除超级管路员');
        }else if(res.code === 300){
            Vue.prototype.$Message.error('该用户没有权限');
        }
        else {
            return response
        }
        // return response
    });

export default service
